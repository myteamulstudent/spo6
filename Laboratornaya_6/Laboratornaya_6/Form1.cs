﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laboratornaya_6
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void PrintResult(Dictionary<string, List<string>> result)
        {
            foreach (KeyValuePair<String, List<String>> pair in result)
            {
                richTextBox1.Text += pair.Key + " : ";
                foreach (string value in pair.Value)
                {
                    richTextBox1.Text += value + " , ";
                }
                richTextBox1.Text += "\r\n";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String expression = textBox1.Text;
            SyntaxAnalyzator sa = new SyntaxAnalyzator();
            Dictionary<string, List<string>> res;
            try
            {
                res = sa.GetAnazyle(expression);
                PrintResult(res);
            }
            catch (Exception ex)
            {
                richTextBox1.Text = ex.Message;
            }
        }
    }
}
