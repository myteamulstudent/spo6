﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Laboratornaya_6
{

    enum keys { ExpressionInBrackets, Digits, Identificator, Logarithm, Result };

    class SyntaxAnalyzator
    {
        private const string digits = "DIGITS";
        private const string identificators = "IDENTIFICATORS";
        private const string expressionInBrackets = "EXPRESSIONSINBRACKETS";
        private const string logarithms = "LOGARITHMS";

        private char[] signs = { '*', '+', '-', '/' };
        private const string correctTag = "__CORRECT__";

        private Dictionary<string, List<string>> result;

        public Dictionary<String, List<String>> GetAnazyle(String arithmeticExpressions)
        {
            result = new Dictionary<string, List<string>>();


            String[] arithmeticExpressionsList = arithmeticExpressions.Split(new char[] { ';' });

            foreach (String wholeExpression in arithmeticExpressionsList)
            {
                if (wholeExpression.Count(c => c == '=') != 1)
                {
                    throw new Exception("Некорректное количество = в выражении " + wholeExpression);
                }

                String[] partExpressions = wholeExpression.Split(new char[] { '=' });


                char leftSideBracket = '(';
                char rightSideBracket = ')';


                for (int i = 0; i < partExpressions.Length; i++)
                {
                    if (string.IsNullOrWhiteSpace(partExpressions[i]))
                    {
                        throw new Exception("Часть выражения, разделенная = не может быть пустой строкой в " + wholeExpression + " --> " + partExpressions[i]);
                    }

                    if (partExpressions[i].Count(c => c == leftSideBracket) != partExpressions[i].Count(c => c == rightSideBracket))
                    {
                        throw new Exception("Некорректно расставлены скобки в выражениии " + wholeExpression.ToString() + " --> " + partExpressions[i]);
                    }

                    Regex regex = new Regex(@"\(([^()]*)\)");

                    while (partExpressions[i].Contains(leftSideBracket))
                    {
                        Match match = regex.Match(partExpressions[i]);
                        while (match.Success)
                        {
                            string value = match.Value.Substring(1, match.Value.Length - 2);

                            checkElementsInPart(match.Value, value, ref partExpressions[i]);

                            match = match.NextMatch();
                        }
                    }

                    string[] elements = partExpressions[i].Split(signs);
                    checkElementsInPart(partExpressions[i], partExpressions[i], ref partExpressions[i]);
                }
            }

            return result;
        }

        private void checkElementsInPart(string valueToReplace, string valueToSplit, ref string partWhereReplace)
        {
            String[] elementsInBrackets = valueToSplit.Split(signs);
            foreach (var element in elementsInBrackets)
            {
                if (IsIdentifier(element))
                {
                    AddValue(identificators, element);
                    partWhereReplace = partWhereReplace.Replace(valueToReplace, correctTag);
                    continue;
                }
                else if (IsNumber(element))
                {
                    AddValue(digits, element);
                    partWhereReplace = partWhereReplace.Replace(valueToReplace, correctTag);
                    continue;
                }
                else if (IsLogarithm(element))
                {
                    AddValue(logarithms, element);
                    partWhereReplace = partWhereReplace.Replace(valueToReplace, correctTag);
                    continue;
                }
                throw new Exception(" Не удалось идентифицировать " + element);
            }
        }
        

        private bool IsLogarithm(string id)
        {
            id = id.Trim();

            if (string.IsNullOrWhiteSpace(id))
            {
                return false;
            }
            if (id == "Math.Log" + correctTag)
                return true;
            return false;
        }

        private bool IsIdentifier(string id)
        {
            id = id.Trim();

            if (id == correctTag)
                return true;

            if (string.IsNullOrEmpty(id))
            {
                return false;
            }

            if (id.Any(c => !char.IsLetterOrDigit(c)))
            {
                return false;
            }

            if (char.IsDigit(id[0]))
            {
                return false;
            }

            return true;
        }

        private bool IsNumber(string value)
        {
            value = value.Trim();

            if (value == correctTag)
                return true;

            if (string.IsNullOrWhiteSpace(value)) {
                return false;
            }

            if (value.All(char.IsDigit))
            {
                return true;
            }

            bool dotsMoreThan1 = value.Count(c => c == '.') > 1;
            bool countOfDotsDoesnotEqualCountOfNotDigits = value.Count(c => !char.IsDigit(c)) != value.Count(c => c == '.');

            if (dotsMoreThan1 || countOfDotsDoesnotEqualCountOfNotDigits)
                return false;

            if (value.Count(c => c == '.') > 1)
                return false;

            if (value.StartsWith(".") || value.EndsWith("."))
                return false;


            return true;
        }

        private void AddValue(string key, string value)
        {
            value = value.Trim();
            if (value == correctTag)
                return;
            if (!result.ContainsKey(key))
            {
                result.Add(key, new List<string>());
            }
            result[key].Add(value);
        }
    }
}
